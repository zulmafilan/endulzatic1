const { Router } = require('express');//me permite definir rutas de navegacion en el backend
var ControllerCliente=require('../src/controllers/ControllerCliente');

const router = Router();

router.get('/prueba',ControllerCliente.prueba);//nombre con el que los visitantes ingresan a: localhost:4000/prueba desde el navegador
//el primer prueba es que busco en el anvegador y el segundo es el que se encuentra en routes
router.post('/crear',ControllerCliente.saveCliente);
router.get('/buscar/:id',ControllerCliente.buscarCliente);
router.get('/buscar/:id?',ControllerCliente.listarAllCliente);
router.delete('/borrarcliente/:id',ControllerCliente.deleteCliente);
router.put('/actualizarcliente/:id',ControllerCliente.updateCliente);

//////////////////////////////////////////////////////////////////////////////////////////////////////

var ControllerProducto=require('../src/controllers/ControllerProducto');
router.get('/pruebap',ControllerProducto.prueba);//nombre con el que los visitantes ingresan a: localhost:4000/prueba desde el navegador
//el primer prueba es que busco en el anvegador y el segundo es el que se encuentra en routes
router.post('/crearp',ControllerProducto.saveProducto);
router.get('/buscarp/:id',ControllerProducto.buscarProducto);
router.get('/buscarp/:id?',ControllerProducto.listarAllProducto);
router.delete('/borrarproducto/:id',ControllerProducto.deleteProducto);
router.put('/actualizarproducto/:id',ControllerProducto.updateProducto);

//////////////////////////////////////////////////////////////////////////////////////////////////////
var ControllerCarrito=require('../src/controllers/ControllerCarrito');
router.get('/pruebap',ControllerCarrito.prueba);//nombre con el que los visitantes ingresan a: localhost:4000/prueba desde el navegador
//el primer prueba es que busco en el anvegador y el segundo es el que se encuentra en routes
router.post('/crearc',ControllerCarrito.saveCarrito);
router.get('/buscarc/:id',ControllerCarrito.buscarCarrito);
router.get('/buscarc/:id?',ControllerCarrito.listarAllCarrito);
router.delete('/borrarcarrito/:id',ControllerCarrito.deleteCarrito);
router.put('/actualizarcarrito/:id',ControllerCarrito.updateCarrito);

module.exports=router;

//export default router;