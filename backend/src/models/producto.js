var mongoose=require('mongoose');
var Schema = mongoose.Schema;
var ProductoSchema=Schema({
   
    idproducto:String,
    tipoproducto:String,
    preciounitario:String,
    valorneto:String,
    valortotal:String,
    email:String
    
    });
const Producto= mongoose.model('producto',ProductoSchema);
module.exports= Producto;