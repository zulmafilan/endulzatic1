const Carrito = require("../models/carrito");
//para obtener un dato de una pestaña
function prueba(req, res) {
    res.status(200).send({ //si el status es 200 en la comunicacion se envia la informacion
        message: 'probando una acción'
    });
}
///para crear un cliente
function saveCarrito(req, res) {
    var myCarrito = new Carrito(req.body);
    myCarrito.save((err, result) => {
        res.status(200).send({ message: result });
    });
}
//para buscar un dato o obtener informacion
function buscarCarrito(req, res) {
    var idCarrito = req.params.id;
    Carrito.findById(idCarrito).exec((err, result) => {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' });
            } else {
                res.status(200).send({ result });
            }
        }
    });
}
function listarAllCarrito(req, res) {
    var idCarrito = req.params.idb;
    if (!idCarrito) {
        var result = Carrito.find({}).sort('nombre');
    } else {
        var result = Carrito.find({ _id: idCliente }).sort('nombre');
    }
    result.exec(function (err, result) {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' })
                    ;
            } else {
                res.status(200).send({ result });
            }
        }
    })
}
//actualizar
const updateCarrito=async (req,res) =>{
    try{
        const id=req.params.id;
        const carrito =req.body;
        await Carrito.findByIdAndUpdate(id,carrito);
        res.json(carrito);
        //res.send("plan actualizado correctamente");


    }
    catch(error){
        res.status(500).json({
            message:error.message
        })
        //console.error(error);
    }
}


//function updateCliente(req, res) {
//    var id = mongoose.Types.ObjectId(req.query.midulcedb);
//    Cliente.findOneAndUpdate({ _id: id }, req.body, { new: true }, function (err,Cliente) {
//        if (err)     
//            res.send(err);
//        res.json(Cliente);
//   });
//};
//borrar
function deleteCarrito(req, res) {
    var idCarrito = req.params.id;
    Carrito.findByIdAndRemove(idCarrito, function (err, carrito) {
        if (err) {
            return res.json(500, {
                message: 'No hemos encontrado el cliente'
            })
        }
        return res.json(carrito)
    });
};

    module.exports = {
        prueba,
        saveCarrito,
        buscarCarrito,
        listarAllCarrito,
        updateCarrito,
        deleteCarrito
    }