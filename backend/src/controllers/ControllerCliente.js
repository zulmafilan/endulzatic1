const Cliente = require("../models/cliente");
//para obtener un dato de una pestaña
function prueba(req, res) {
    res.status(200).send({ //si el status es 200 en la comunicacion se envia la informacion
        message: 'probando una acción'
    });
}
///para crear un cliente
function saveCliente(req, res) {
    var myCliente = new Cliente(req.body);
    myCliente.save((err, result) => {
        res.status(200).send({ message: result });
    });
}
//para buscar un dato o obtener informacion
function buscarCliente(req, res) {
    var idCliente = req.params.id;
    Cliente.findById(idCliente).exec((err, result) => {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' });
            } else {
                res.status(200).send({ result });
            }
        }
    });
}
function listarAllCliente(req, res) {
    var idCliente = req.params.idb;
    if (!idCliente) {
        var result = Cliente.find({}).sort('nombre');
    } else {
        var result = Cliente.find({ _id: idCliente }).sort('nombre');
    }
    result.exec(function (err, result) {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' })
                    ;
            } else {
                res.status(200).send({ result });
            }
        }
    })
}
//actualizar
const updateCliente=async (req,res) =>{
    try{
        const id=req.params.id;
        const cliente =req.body;
        await Cliente.findByIdAndUpdate(id,cliente);
        res.json(cliente);
        //res.send("plan actualizado correctamente");


    }
    catch(error){
        res.status(500).json({
            message:error.message
        })
        //console.error(error);
    }
}


//function updateCliente(req, res) {
//    var id = mongoose.Types.ObjectId(req.query.midulcedb);
//    Cliente.findOneAndUpdate({ _id: id }, req.body, { new: true }, function (err,Cliente) {
//        if (err)     
//            res.send(err);
//        res.json(Cliente);
//   });
//};
//borrar
function deleteCliente(req, res) {
    var idCliente = req.params.id;
    Cliente.findByIdAndRemove(idCliente, function (err, cliente) {
        if (err) {
            return res.json(500, {
                message: 'No hemos encontrado el cliente'
            })
        }
        return res.json(cliente)
    });
};

    module.exports = {
        prueba,
        saveCliente,
        buscarCliente,
        listarAllCliente,
        updateCliente,
        deleteCliente
    }