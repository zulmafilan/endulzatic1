const Producto = require("../models/producto");
//para obtener un dato de una pestaña
function prueba(req, res) {
    res.status(200).send({ //si el status es 200 en la comunicacion se envia la informacion
        message: 'probando una acción'
    });
}
///para crear un producto
function saveProducto(req, res) {
    var myProducto = new Producto(req.body);
    myProducto.save((err, result) => {
        res.status(200).send({ message: result });
    });
}
//para buscar un dato o obtener informacion
function buscarProducto(req, res) {
    var idProducto = req.params.id;
    Producto.findById(idProducto).exec((err, result) => {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' });
            } else {
                res.status(200).send({ result });
            }
        }
    });
}
function listarAllProducto(req, res) {
    var idProducto = req.params.idb;
    if (!idProducto) {
        var result = Producto.find({}).sort('nombre');
    } else {
        var result = Producto.find({ _id: idProducto }).sort('nombre');
    }
    result.exec(function (err, result) {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' })
                    ;
            } else {
                res.status(200).send({ result });
            }
        }
    })
}
//actualizar
const updateProducto=async (req,res) =>{
    try{
        const id=req.params.id;
        const producto =req.body;
        await Producto.findByIdAndUpdate(id,producto);
        res.json(producto);
        //res.send("plan actualizado correctamente");


    }
    catch(error){
        res.status(500).json({
            message:error.message
        })
        //console.error(error);
    }
}


//function updateCliente(req, res) {
//    var id = mongoose.Types.ObjectId(req.query.midulcedb);
//    Cliente.findOneAndUpdate({ _id: id }, req.body, { new: true }, function (err,Cliente) {
//        if (err)     
//            res.send(err);
//        res.json(Cliente);
//   });
//};
//borrar
function deleteProducto(req, res) {
    var idProducto = req.params.id;
    Producto.findByIdAndRemove(idProducto, function (err, producto) {
        if (err) {
            return res.json(500, {
                message: 'No hemos encontrado el cliente'
            })
        }
        return res.json(producto)
    });
};

    module.exports = {
        prueba,
        saveProducto,
        buscarProducto,
        listarAllProducto,
        updateProducto,
        deleteProducto
    }